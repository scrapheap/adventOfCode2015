use Modern::Perl;
use Test::More;
use Test::Deep;

use Day_18 qw(
    parseGrid
    nextGrid
    turnOnCorners
);

use Readonly;

my @initialGrid = split /\n/, <<EOG;
.#.#.#
...##.
#....#
..#...
#.#..#
####..
EOG

subtest 'Part 1' => sub {
    is_deeply(
        [ parseGrid( @initialGrid ) ],
        [
            [ 0, 1, 0, 1, 0, 1 ],
            [ 0, 0, 0, 1, 1, 0 ],
            [ 1, 0, 0, 0, 0, 1 ],
            [ 0, 0, 1, 0, 0, 0 ],
            [ 1, 0, 1, 0, 0, 1 ],
            [ 1, 1, 1, 1, 0, 0 ],
        ],
        "A parsed grid should have values of 1 or 0"
    );

    is_deeply(
        [ nextGrid(
            [ 0, 1, 0, 1, 0, 1 ],
            [ 0, 0, 0, 1, 1, 0 ],
            [ 1, 0, 0, 0, 0, 1 ],
            [ 0, 0, 1, 0, 0, 0 ],
            [ 1, 0, 1, 0, 0, 1 ],
            [ 1, 1, 1, 1, 0, 0 ],
        ) ],
        [
            [ 0, 0, 1, 1, 0, 0 ],
            [ 0, 0, 1, 1, 0, 1 ],
            [ 0, 0, 0, 1, 1, 0 ],
            [ 0, 0, 0, 0, 0, 0 ],
            [ 1, 0, 0, 0, 0, 0 ],
            [ 1, 0, 1, 1, 0, 0 ],
        ],
        "The next grid should moved on one step"
    )
};


subtest 'Part 2' => sub {
    is_deeply(
        [ turnOnCorners(
            [ 0, 1, 0, 1, 0, 0 ],
            [ 0, 0, 0, 1, 1, 0 ],
            [ 1, 0, 0, 0, 0, 1 ],
            [ 0, 0, 1, 0, 0, 0 ],
            [ 1, 0, 1, 0, 0, 1 ],
            [ 0, 1, 1, 1, 0, 0 ],
        ) ],
        [
            [ 1, 1, 0, 1, 0, 1 ],
            [ 0, 0, 0, 1, 1, 0 ],
            [ 1, 0, 0, 0, 0, 1 ],
            [ 0, 0, 1, 0, 0, 0 ],
            [ 1, 0, 1, 0, 0, 1 ],
            [ 1, 1, 1, 1, 0, 1 ],
        ],
        "The new grid should have its coreners turned on"
    )
};

done_testing();
