use Modern::Perl;
use Test::More;
use Test::Deep;

use Day_11 qw(
    validCharacters  containsStraight  containsAtLeastTwoDoubles  nextValidPassword
    jumpToEndOfInvalidCharacterRange
);

ok( ! validCharacters( "abcdefghijkmnpqrstuvwxyz" ), "`i` is not a valid character"  );
ok( ! validCharacters( "abcdefghjklmnpqrstuvwxyz" ), "`l` is not a valid character" );
ok( ! validCharacters( "abcdefghjkmnopqrstuvwxyz" ), "`o` is not a valid character" );
ok(   validCharacters( "abcdefghjkmnpqrstuvwxyz" ),  "All other characters are valid" );

ok( ! containsStraight( "abbceffg" ), "abbceffg doesn't contain a straight" );
ok( ! containsStraight( "abbcegjk" ), "abbcegjk doesn't contain a straight" );
ok(   containsStraight( "ghjaabcc" ), "ghjaabcc does contain a straight" );
ok(   containsStraight( "hijklmmn" ), "hijklmmn does contain a straight" );

ok( ! containsAtLeastTwoDoubles( "ablcegjk" ), "ablcegjk doesn't contain any doubles" );
ok( ! containsAtLeastTwoDoubles( "abbcegjk" ), "abbcegjk doesn't contain two doubles" );
ok(   containsAtLeastTwoDoubles( "abbceffg" ), "abbceffg contains two doubles (bb and ff)" );

is( jumpToEndOfInvalidCharacterRange( "ghijklmn" ), "ghizzzzz", "We should jump to the end of the invalid characters range" );

is( nextValidPassword( "ghjaabcb" ),  "ghjaabcc", "The next valid password after ghijklmn should be ghjaabcc" );
is( nextValidPassword( "abcdefgh" ),  "abcdffaa", "The next valid password after abcdefgh should be abcdffaa" );
is( nextValidPassword( "ghijklmn" ),  "ghjaabcc", "The next valid password after ghijklmn should be ghjaabcc" );

done_testing();
