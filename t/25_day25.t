use Modern::Perl;
use Test::More;
use Test::Deep;

use Day_25 qw(
    nextCode

    codeAt
);

subtest 'Part 1' => sub {
    is( nextCode( 20151125 ), 31916031 , "The next code after 20151125 should be 31916031" );
    is( nextCode( 31916031 ), 18749137 , "The next code after 31916031 should be 18749137" );

    is( codeAt( 1, 1 ), 20151125, "The code at 1,1 is the initial seed" );
    is( codeAt( 2, 1 ), 31916031, "The code at 2,1 is the second one" );
    is( codeAt( 1, 2 ), 18749137, "The code at 1,2 is the third one" );
    is( codeAt( 3, 1 ), 16080970, "The code at 3,1 is the fourth one" );
    is( codeAt( 2, 2 ), 21629792, "The code at 2,2 is the fifth one" );
    is( codeAt( 1, 3 ), 17289845, "The code at 1,3 is the sixth one" );

};

done_testing();
