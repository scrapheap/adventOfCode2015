use Modern::Perl;
use Test::More;
use Test::Deep;

use Day_21 qw(
    fight  equipmentGenerator

    BOSS_WINS  PLAYER_WINS
);

subtest 'fight' => sub {
    my $player = { armour => 5, damage => 5, hp => 8  };
    my $boss   = { armour => 2, damage => 7, hp => 12 };

    is( fight( $player, $boss ), PLAYER_WINS, "Player should win" );

    $player = { armour => 5, damage => 5, hp => 8  };
    $boss   = { armour => 2, damage => 7, hp => 20 };

    is( fight( $player, $boss ), BOSS_WINS, "Boss should win" );
};

subtest 'equipmentGenerator' => sub {
    my $nextTry = equipmentGenerator(
        weapons => [
            { name => 'dagger',     cost =>  8, damage => 4, armour => 0 },
            { name => 'shorsword',  cost => 10, damage => 5, armour => 0 },
        ],
        armour => [
            { name => 'leather',    cost => 13, damage => 0, armour => 1 },
            { name => 'chainmail',  cost => 31, damage => 0, armour => 2 },
        ],
        rings => [
            { name => 'damage +1',  cost => 25, damage => 1, armour => 0 },
            { name => 'defense +1', cost => 20, damage => 0, armour => 1 },
        ],
    );

    my @permutations;
    while( my $try = $nextTry->() ) {
        push @permutations, $try;
    }
     
    cmp_deeply(
        \@permutations,
        bag(
            { equipment => ignore(), cost => (  8 ),                damage => ( 4 ),     armour => ( 0 )             }, # dagger only
            { equipment => ignore(), cost => (  8 + 13 ),           damage => ( 4 ),     armour => ( 0 + 1 )         }, # dagger + leather
            { equipment => ignore(), cost => (  8 + 31 ),           damage => ( 4 ),     armour => ( 0 + 2 )         }, # dagger + chainmail
            { equipment => ignore(), cost => (  8 + 25 ),           damage => ( 4 + 1 ), armour => ( 0 + 0 )         }, # dagger + damage_1
            { equipment => ignore(), cost => (  8 + 20 ),           damage => ( 4 ),     armour => ( 0 + 1 )         }, # dagger + defense_1
            { equipment => ignore(), cost => (  8 + 25 + 20 ),      damage => ( 4 + 1 ), armour => ( 0 + 0 +  1 )    }, # dagger + damage_1 + defenese_1
            { equipment => ignore(), cost => (  8 + 13 + 25 ),      damage => ( 4 + 1 ), armour => ( 0 + 1 + 0 )     }, # dagger + leather + damage_1
            { equipment => ignore(), cost => (  8 + 13 + 20 ),      damage => ( 4 ),     armour => ( 0 + 1 + 1 )     }, # dagger + leather + defense_1
            { equipment => ignore(), cost => (  8 + 13 + 25 + 20 ), damage => ( 4 + 1 ), armour => ( 0 + 1 + 0 + 1 ) }, # dagger + leather + damage_1 + defenese_1
            { equipment => ignore(), cost => (  8 + 31 + 25 ),      damage => ( 4 + 1 ), armour => ( 0 + 2 + 0 )     }, # dagger + chainmail + damage_1
            { equipment => ignore(), cost => (  8 + 31 + 20 ),      damage => ( 4 ),     armour => ( 0 + 2 + 1 )     }, # dagger + chainmail + defense_1
            { equipment => ignore(), cost => (  8 + 31 + 25 + 20 ), damage => ( 4 + 1 ), armour => ( 0 + 2 + 0 + 1 ) }, # dagger + chainmail + damage_1 + defenese_1
            { equipment => ignore(), cost => ( 10 ),                damage => ( 5 ),     armour => ( 0 )             }, # shortsword only
            { equipment => ignore(), cost => ( 10 + 13 ),           damage => ( 5 ),     armour => ( 0 + 1 )         }, # shortsword + leather
            { equipment => ignore(), cost => ( 10 + 31 ),           damage => ( 5 ),     armour => ( 0 + 2 )         }, # shortsword + chainmail
            { equipment => ignore(), cost => ( 10 + 25 ),           damage => ( 5 + 1 ), armour => ( 0 + 0 )         }, # shortsword + damage_1
            { equipment => ignore(), cost => ( 10 + 20 ),           damage => ( 5 ),     armour => ( 0 + 1 )         }, # shortsword + defense_1
            { equipment => ignore(), cost => ( 10 + 25 + 20 ),      damage => ( 5 + 1 ), armour => ( 0 + 0 +  1 )    }, # shortsword + damage_1 + defenese_1
            { equipment => ignore(), cost => ( 10 + 13 + 25 ),      damage => ( 5 + 1 ), armour => ( 0 + 1 + 0 )     }, # shortsword + leather + damage_1
            { equipment => ignore(), cost => ( 10 + 13 + 20 ),      damage => ( 5 ),     armour => ( 0 + 1 + 1 )     }, # shortsword + leather + defense_1
            { equipment => ignore(), cost => ( 10 + 13 + 25 + 20 ), damage => ( 5 + 1 ), armour => ( 0 + 1 + 0 + 1 ) }, # shortsword + leather + damage_1 + defenese_1
            { equipment => ignore(), cost => ( 10 + 31 + 25 ),      damage => ( 5 + 1 ), armour => ( 0 + 2 + 0 )     }, # shortsword + chainmail + damage_1
            { equipment => ignore(), cost => ( 10 + 31 + 20 ),      damage => ( 5 ),     armour => ( 0 + 2 + 1 )     }, # shortsword + chainmail + defense_1
            { equipment => ignore(), cost => ( 10 + 31 + 25 + 20 ), damage => ( 5 + 1 ), armour => ( 0 + 2 + 0 + 1 ) }, # shortsword + chainmail + damage_1 + defenese_1
        ),
        "We should see all the permutations that are possible"
    );

};

done_testing();
