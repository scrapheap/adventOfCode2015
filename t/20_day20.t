use Modern::Perl;
use Test::More;
use Test::Deep;

use Day_20 qw(
    firstHouseToGetAtLeast
    lazyElvesFirstHouseToGetAtLeast
);

use Readonly;

subtest 'Part 1' => sub {
    is( firstHouseToGetAtLeast( 120, 100 ), 6, "The first house to get at least 120 presents should be 6" );
    is( firstHouseToGetAtLeast( 150, 100 ), 8, "The first house to get at least 150 presents should be 8" );
    
};


subtest 'Part 2' => sub {
    is( lazyElvesFirstHouseToGetAtLeast( 120, 100 ), 6, "The first house to get at least 120 presents should be 6" );
    is( lazyElvesFirstHouseToGetAtLeast( 165, 100 ), 8, "The first house to get at least 165 presents should be 8" );
    is( lazyElvesFirstHouseToGetAtLeast( 198, 100 ), 10, "The first house to get at least 198 presents should be 8" );
};

done_testing();
