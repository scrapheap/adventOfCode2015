use Modern::Perl;
use Test::More;
use Test::Deep;

use Day_19 qw(
    parseReplacements
    allPossibleReplacements

    invertedParseReplacements
    stepsTo
);

use Readonly;

my @replacements = split /\n/, <<EOG;
H => HO
H => OH
O => HH
EOG

subtest 'Part 1' => sub {
    my $replacements = parseReplacements( @replacements );

    is_deeply(
        $replacements,
        {
            H => [ 'HO', 'OH' ],
            O => [ 'HH' ],
        },
        "Parsed replacements should be a hash of arrays"
    );

    cmp_deeply(
        [ allPossibleReplacements( $replacements, "HOH" ) ],
        bag(
            "HOOH",
            "HOHO",
            "OHOH",
            "HOOH",
            "HHHH",
        ),
        "We should get a set of all possible values after replacement"
    );
};


subtest 'Part 2' => sub {
    my $replacements = invertedParseReplacements( "e => O", "e => H", @replacements );

    is_deeply(
        $replacements,
        {
            H  => 'e',
            O  => 'e',
            HO => 'H',
            OH => 'H',
            HH => 'O',
        },
        "Inverted parse of replacements should be a normal hash of strings"
    );
   
    is( stepsTo( $replacements, "HOH" ), 3, "Three steps to get to HOH from e" );
    is( stepsTo( $replacements, "HOHOHO" ), 6, "Six steps to get to HOHOHO from e" );
};

done_testing();
