use Modern::Perl;
use Test::More;
use Test::Deep;

use Day_16 qw(
    createFilter
    createFilter2
);

use Readonly;

Readonly my @TEST => split /\n/, <<EOT;
children: 3
cats: 7
samoyeds: 2
pomeranians: 3
akitas: 0
vizslas: 0
goldfish: 5
trees: 3
cars: 2
perfumes: 1
EOT

subtest 'Part 1' => sub {
    my $filter = createFilter( @TEST );

    is( ref $filter, "CODE", "We should get a function back" );

    my @happyTests = (
        "sue 1: children: 3",
    );

    my @unhappyTests = (
        "sue 2: children: 8",
        "sue 3: children: 1",
    );

    foreach my $test ( @happyTests ) {
        ok ( $filter->( $test ), "'$test' should meet the criteria" );
    }

    foreach my $test ( @unhappyTests ) {
        ok ( ! $filter->( $test ), "'$test' should not meet the criteria" );
    }
};

subtest 'Part 2' => sub {
    my $filter = createFilter2( @TEST );

    is( ref $filter, "CODE", "We should get a function back" );

    my @happyTests = (
        "sue 1: children: 3",
        "sue 1: trees: 4",
        "sue 1: cats: 8",
        "sue 1: pomeranians: 2",
        "sue 1: goldfish: 4",
    );

    my @unhappyTests = (
        "sue 1: trees: 3",
        "sue 1: cats: 7",
        "sue 1: pomeranians: 3",
        "sue 1: goldfish: 5",
        "sue 2: children: 8",
        "sue 3: children: 1",
    );

    foreach my $test ( @happyTests ) {
        ok ( $filter->( $test ), "'$test' should meet the criteria" );
    }

    foreach my $test ( @unhappyTests ) {
        ok ( ! $filter->( $test ), "'$test' should not meet the criteria" );
    }

};

done_testing();
