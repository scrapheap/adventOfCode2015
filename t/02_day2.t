use Modern::Perl;
use Test::More;

use Day_02 qw( paperRequirement ribbonRequirement );

is( paperRequirement( "2x3x4" ), 58, "a 2x3x4 present requires 58 square feet of paper");
is( paperRequirement( "1x1x10" ), 43, "a 1x1x10 persent requires 43 square feet of paper");

is( ribbonRequirement( "2x3x4" ), 34 , "a 2x3x4 present requires 34 feet of ribbon");
is( ribbonRequirement( "1x1x10" ), 14, "a 1x1x10 present requires 14 feet of ribbon");

done_testing();
