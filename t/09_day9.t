use Modern::Perl;
use Test::More;
use Test::Deep;
use Readonly;

use Day_09 qw( shortestDistance longestDistance );

Readonly my @DISTANCES => split /\n/, <<EOS;
London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141
EOS

is( shortestDistance( @DISTANCES ), 605, "The shortest distance should be returned" );

is( longestDistance( @DISTANCES ), 982, "The longest distance should be returned" );

done_testing();
