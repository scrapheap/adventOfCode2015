use Modern::Perl;
use Test::More;
use Test::Deep;

use Day_12 qw(
    sumNumbers
    removeRedObjects
);

use Readonly;

Readonly my %TESTS => (
    '[1,2,3]'              => 6,
    '{"a":2,"b":4}'        => 6,
    '[[[3]]]'              => 3,
    '{"a":{"b":4},"c":-1}' => 3,
    '{"a":[-1,1]}'         => 0,
    '[-1,{"a":1}]'         => 0,
    '[]'                   => 0,
    '{}'                   => 0,
    
    '[1,{"c":"red","b":2},3]'         => 4,
    '{"d":"red","e":[1,2,3,4],"f":5}' => 0,
    '[1,"red",5]'                     => 6,
);

foreach my $jsonString ( keys %TESTS ) {
    is( sumNumbers( removeRedObjects( $jsonString ) ), $TESTS{ $jsonString } , "$jsonString should return sum of $TESTS{ $jsonString }" );
}

done_testing();
