use Modern::Perl;
use Test::More;
use Test::Deep;

use Day_03 qw( deliverPresents splitRoute );

cmp_deeply(
    deliverPresents(">"),
    {
        0 => {
            0 => 1,
        },
        1 => {
            0 => 1,
        },
    },
    "After following the pattern we should have a present count that fits what we expect"
);

cmp_deeply(
    deliverPresents("^>v<"),
    {
        0 => {
            0 => 2,
            1 => 1,
        },
        1 => {
            0 => 1,
            1 => 1,
        },
    },
    "After following the pattern `^>v<` we should have a present count we expect"
);

cmp_deeply(
    deliverPresents("^v^v^v^v^v"),
    {
        0 => {
            0 => 6,
            1 => 5,
        },
    },
    "After following the pattern `^v^v^v^v^v` we should have a present count we expect"
);

cmp_deeply(
    [ splitRoute(">") ],
    [
        '>',
        '',
    ],
    "After spliting the route `>` we should have the routes we expect, including an empty route"
);

cmp_deeply(
    [ splitRoute("^v") ],
    [
        '^',
        'v',
    ],
    "After spliting the route `^v` we should have the routes we expect"
);

cmp_deeply(
    [ splitRoute("^>v<") ],
    [
        '^v',
        '><',
    ],
    "After spliting the route `^>v<` we should have the routes we expect"
);

cmp_deeply(
    [ splitRoute("^v^v^v^v^v") ],
    [
        '^^^^^',
        'vvvvv',
    ],
    "After spliting the route `^v^v^v^v^v` we should have the routes we expect"
);

done_testing();
