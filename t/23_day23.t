use Modern::Perl;
use Test::More;
use Test::Deep;

use Day_23 qw(
    runCode
);

my @example = split /\n/, <<EOC;
inc a
jio a, +2
tpl a
inc a
EOC


subtest 'Part 1' => sub {
    is_deeply(
        [ runCode( @example ) ],
        [ 2, 0 ],
        "our first example should set register a to 2"
    );
};

done_testing();
