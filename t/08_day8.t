use Modern::Perl;
use Test::More;
use Test::Deep;
use Readonly;

use Day_08 qw( escapeString  unescapeString  totalStringsSize  totalMemoryUsed );

Readonly my $B => q{\\};

is( unescapeString( q{""} ), '', "The string should be empty" );
is( unescapeString( q{"abc"} ), 'abc', "The string should just be abc" );
is( unescapeString( q{"aaa\"aaa"} ), 'aaa"aaa', 'The string should just be aaa"aaa' );
is( unescapeString( q{"\x27"} ), "'", "The string should just a '" );
is( unescapeString( q{"\x27fd"} ), "'fd", "The string should be 'fd" );

is( escapeString( qq{""} ), qq{"$B"$B""}, "The string have had the double quotes escaped" );
is( escapeString( qq{"abc"} ), qq{"$B"abc$B""}, "The string should also have had the double quotes escaped" );
is( escapeString( qq{"aaa$B"aaa"} ), qq{"$B"aaa$B$B$B"aaa$B""}, 'The string should have all the double quotes and backslashes escaped' );
is( escapeString( qq{"${B}x27"} ), qq{"$B"$B${B}x27$B""}, "The string should have all the double quotes and backslashes escaped '" );
is( escapeString( qq{"njro${B}x68qgbx${B}xe4af${B}"${B}${B}suan"} ), qq{"$B"njro$B${B}x68qgbx$B${B}xe4af$B$B$B"$B$B$B${B}suan$B""}, "The string should have all the double quotes and backslashes escaped '" );

Readonly my @STRINGS => (
    q{""},
    q{"abc"},
    q{"aaa\"aaa"},
    q{"\x27"},
);

is( totalStringsSize( @STRINGS ), 23, "The total number of characters in the strings should be returned" );

is( totalMemoryUsed( @STRINGS ), 11, "The total number of bytes of memeory used should be returned" );

Readonly my @ESCAPED_STRINGS => map { escapeString( $_ ) } @STRINGS;

is( totalStringsSize( @ESCAPED_STRINGS ), 42, "The total string size for the doubly escaped strings shoudl be 42" );

done_testing();
