use Modern::Perl;
use Test::More;
use Test::Deep;

use Day_17 qw(
    findAllPermutations
    findSmallestSets
);

use Readonly;

my @containers = split /\n/, <<EOC;
20
15
10
5
5
EOC

subtest 'Part 1' => sub {
    cmp_deeply(
        [ findAllPermutations( 25, @containers ) ],
        bag(
            [ 15, 10 ],
            [ 20,  5 ],
            [ 20,  5 ],
            [ 15,  5,  5 ],
        ),
        "We should find all 4 valid permutations"
    )

};


subtest 'Part 2' => sub {
    cmp_deeply(
        [ findSmallestSets(
            [ 15, 10 ],
            [ 20,  5 ],
            [ 20,  5 ],
            [ 15,  5,  5 ],
        ) ],
        bag(
            [ 15, 10 ],
            [ 20,  5 ],
            [ 20,  5 ],
        ),
        "We should find the sets with the minimum size"
    );
};

done_testing();
