use Modern::Perl;
use Test::More;

use Day_04 qw( findValidMD5 );

is( findValidMD5('abcdef'), 609043, "The valid input for abcdef is 609043" );
is( findValidMD5('pqrstuv'), 1048970, "The valid input for pqrstuv is 1048970" );

done_testing();
