use Modern::Perl;
use Test::More;
use Test::Deep;

use Day_14 qw(
    calculateMaxDistance  calculatePoints
);

use Readonly;

Readonly my @TEST => split /\n/, <<EOT;
Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.
EOT

is( calculateMaxDistance(   10, @TEST ),  160, "The max distance one could get after 10 seconds should be 160" );
is( calculateMaxDistance( 1000, @TEST ), 1120, "The max distance one could get after 1000 seconds should be 1120" );

is( calculatePoints(  139, @TEST ) , 139, "The leaders points at 139 seconds should be 139" );
is( calculatePoints( 1000, @TEST ) , 689, "The leaders points at 1000 seconds should be 689" );

done_testing();
