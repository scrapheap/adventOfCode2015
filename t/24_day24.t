use Modern::Perl;
use Test::More;
use Test::Deep;

use Day_24 qw(
    quantumEntanglement
    optimalSplit
    optimalSplitWithTrunk
);

my @example = ( 1..5, 7..11 );

subtest 'Part 1' => sub {
    is( quantumEntanglement( 11, 9 ) , 99,  "The quantum entanglement of 11 and 9 is 99" );
    is( quantumEntanglement( 10, 9, 1 ) , 90,  "The quantum entanglement of 10, 9 and 1 is 90" );
    
    my ( $group1, $group2, $group3 ) = optimalSplit( @example );

    cmp_deeply(
        $group1,
        bag( 11, 9 ),
        "The first group should be the smallest"
    );

    cmp_deeply(
        [ $group2, $group3 ],
        bag(
            bag( 10, 8, 2 ),
            bag( 7, 5, 4, 3, 1 ),
        ),
        "The other two groups should be the same size but order doesn't matter"
    );
};

subtest 'Part 2' => sub {
    my ( $group1 ) = optimalSplitWithTrunk( @example );

    cmp_deeply(
        $group1,
        bag( 11, 4 ),
        "The first group should be the smallest"
    );
};

done_testing();
