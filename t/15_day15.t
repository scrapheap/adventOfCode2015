use Modern::Perl;
use Test::More;
use Test::Deep;

use Day_15 qw(
    calculateMaxTastiness
    calculateMaxTastinessForCalories
);

use Readonly;

Readonly my @TEST => split /\n/, <<EOT;
Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3
EOT

is( calculateMaxTastiness( @TEST ),  62842880, "The max tastiness should be 62842880" );

is( calculateMaxTastinessForCalories( 500, @TEST ),  57600000, "The max tastiness for 500 calories should be 57600000" );

done_testing();
