use Modern::Perl;
use Test::More;
use Test::Deep;

use Day_10 qw( lookAndSay );

is( lookAndSay( 1 ), "11", "one should be said as one one" );
is( lookAndSay( 11 ), "21", "one one should be said as two ones" );
is( lookAndSay( 21 ), "1211", "two one should be said as one two and two ones" );
is( lookAndSay( 1211 ), "111221", "one two one one should be said as one one, one two and two ones" );
is( lookAndSay( 111221 ), "312211", "one one one two two one should be said as three ones, two twos and one one" );

done_testing();
