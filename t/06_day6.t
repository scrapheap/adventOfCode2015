use Modern::Perl;
use Test::More;
use Test::Deep;

use Day_06 qw( region parseLine );

cmp_deeply(
    [ region( 0, 0, 2, 2 ) ],
    [ [ 0, 0 ], [ 1, 0 ], [ 2, 0 ],
      [ 0, 1 ], [ 1, 1 ], [ 2, 1 ],
      [ 0, 2 ], [ 1, 2 ], [ 2, 2 ],
    ],
    "We should get back an array of co-ordinates for each cell in the specified region",
);

cmp_deeply(
    [ region( 0, 1, 3, 1 ) ],
    [ [ 0, 1 ], [ 1, 1 ], [ 2, 1 ], [ 3, 1 ] ],
    "We should get back an array of co-ordinates for each cell in the specified region",
);

cmp_deeply(
    parseLine("turn on 0,0 through 999,999"),
    {
        op => "turn on",
        from => [0, 0],
        to => [999, 999],
    },
    "Parsed line should return the values we expect"
);

cmp_deeply(
    parseLine("toggle 0,0 through 999,0"),
    {
        op => "toggle",
        from => [0, 0],
        to => [999, 0],
    },
    "Parsed line should return the values we expect"
);

cmp_deeply(
    parseLine("turn off 499,499 through 500,500"),
    {
        op => "turn off",
        from => [499, 499],
        to => [500, 500],
    },
    "Parsed line should return the values we expect"
);

done_testing();
