use Modern::Perl;
use Test::More;
use Test::Deep;

use Day_22 qw(
    findEfficientWinCost
);

subtest 'Part 1' => sub {
    my $boss = { hp => 13, damage => 8 };
    my $player = { hp => 10, mana => 250 };

    is( findEfficientWinCost( $player, $boss ), 226, "First kill should cost us 226" );

    $boss = { hp => 14, damage => 8 };
    $player = { hp => 10, mana => 250 };

    is( findEfficientWinCost( $player, $boss ), 641, "Second kill should cost us 641" );
};

done_testing();
