use Modern::Perl;
use Test::More;
use Test::Deep;
use Readonly;

use Day_07 qw( runCircuit );

my @SCHEMA = split /\n/, <<EOS;
456 -> y
x AND y -> d
x OR y -> e
x LSHIFT 2 -> f
y RSHIFT 2 -> g
NOT x -> h
NOT y -> i
123 -> x
EOS

cmp_deeply(
    { runCircuit( @SCHEMA ) },
    {
        d => 72,
        e => 507,
        f => 492,
        g => 114,
        h => 65412,
        i => 65079,
        x => 123,
        y => 456,
    },
    "Running the circuit should produce the expected hash"
);


done_testing();
