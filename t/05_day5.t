use Modern::Perl;
use Test::More;

use Day_05 qw( nice nice2 );

ok( nice('ugknbfddgicrmopn'), "'ugknbfddgicrmopn' should be nice" );
ok( nice('aaa'), "'aaa' should be nice" );
ok( ! nice('jchzalrnumimnmhp'), "'jchzalrnumimnmhp' should not be nice" );
ok( ! nice('haegwjzuvuyypxyu'), "'haegwjzuvuyypxyu' should not be nice" );
ok( ! nice('dvszwmarrgswjxmb'), "'dvszwmarrgswjxmb' should not be nice" );

ok( nice2('qjhvhtzxzqqjkmpb'), "'qjhvhtzxzqqjkmpb' should be nice" );
ok( nice2('xxyxx'), "'xxyxx' should be nice" );
ok( ! nice2('uurcxstgmygtbstg'), "'uurcxstgmygtbstg' should not be nice" );
ok( ! nice2('ieodomkazucvgmuy'), "'ieodomkazucvgmuy' should not be nice" );

done_testing();
