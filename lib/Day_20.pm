package Day_20;

use Modern::Perl;

use Exporter qw( import );

our @EXPORT_OK = qw(
    firstHouseToGetAtLeast
    lazyElvesFirstHouseToGetAtLeast
);

sub firstHouseToGetAtLeast {
    my ( $target, $size ) = @_;

    my @houses;

    for( my $elf = 1; $elf < $size; $elf++) {
        my $currentHouse = $elf;
        my $presents = $elf * 10;

        while( $currentHouse < $size ) {
            $houses[ $currentHouse ] += $presents;

            $currentHouse += $elf;
        }
    }

    for( my $house = 1; $house < $size; $house++ ) {
        if ( $houses[ $house ] >= $target ) {
            return $house;
        }
    }
}


sub lazyElvesFirstHouseToGetAtLeast {
    my ( $target, $size ) = @_;

    my @houses;

    for( my $elf = 1; $elf < $size; $elf++) {
        my $currentHouse = $elf;
        my $presents = $elf * 11;
        my $limit = 50;

        while( $limit  &&  $currentHouse < $size ) {
            $houses[ $currentHouse ] += $presents;

            $currentHouse += $elf;
            $limit--;
        }
    }

    for( my $house = 1; $house < $size; $house++ ) {
        if ( $houses[ $house ] >= $target ) {
            return $house;
        }
    }
}

1;
