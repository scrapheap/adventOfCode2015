package Day_12;

use Modern::Perl;

use Exporter qw( import );

our @EXPORT_OK = qw(
    sumNumbers
    removeRedObjects
);

use List::Util qw( sum0 );
use JSON qw( decode_json  encode_json );


sub sumNumbers {
    my ( $string ) = @_;

    my @numbers = $string =~ m/(-?\d+)/g;

    return sum0( @numbers );
}


sub removeRedObjects {
    my ( $string ) = @_;

    my $data = decode_json( $string );

    return encode_json( filterReds( $data ) );
}


sub filterReds {
    my ( $node ) = @_;

    if( ref $node eq "ARRAY" ) {
        foreach my $elem ( @$node ) {
            $elem = filterReds( $elem );
        }

    } elsif ( ref $node eq "HASH" ) {
        foreach my $key ( keys %$node ) {
            if ( ref $node->{ $key } ) {
                $node->{ $key } = filterReds( $node->{ $key } );
            } elsif ( $node->{ $key } eq "red" ) {
                return {};
            }
        }
    }

    return $node;
}

1;
