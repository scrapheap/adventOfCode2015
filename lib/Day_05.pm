package Day_05;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( nice nice2 );

sub nice {
    my ($string) = @_;

    return $string =~ m/[aeiou].*[aeiou].*[aeiou]/
        && $string =~ m/(\w)\1/
        && $string !~ m/ab|cd|pq|xy/;
}

sub nice2 {
    my ($string) = @_;

    return $string =~ m/(\w\w).*?\1/
        && $string =~ m/(\w).\1/;
}

1;
