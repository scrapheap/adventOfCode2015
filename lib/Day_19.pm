package Day_19;

use Modern::Perl;

use Exporter qw( import );

our @EXPORT_OK = qw(
    parseReplacements
    allPossibleReplacements

    invertedParseReplacements
    stepsTo
);

use List::Util qw( reduce max );

sub parseReplacements {
    return reduce {
        my ( $from, $to ) = $b =~ m/(\w+)\s*=>\s*(\w+)/;
        push @{ $a->{ $from } }, $to;
        $a
    } {}, @_;
}


sub allPossibleReplacements {
    my ( $replacements, $molecule ) = @_;

    my $maxLength = max( map { length } keys %$replacements );

    my @possibilities;

    for( my $i = 0; $i < length( $molecule ); $i++ ) {
        foreach my $length ( 1..$maxLength ) {
            next if $length + $i > length( $molecule );

            my $left = substr( $molecule, 0, $i );
            my $char = substr( $molecule, $i, $length );
            my $right = substr( $molecule, $i + $length );

            foreach my $replacement ( @{$replacements->{ $char } } ) {
                push @possibilities, $left . $replacement . $right;
            }
        }
    }

    return @possibilities;
}


sub invertedParseReplacements {
    return {
        map {
            my ( $from, $to ) = $_ =~ m/(\w+)\s*=>\s*(\w+)/;
            ( $to => $from );
        } @_
    };
}


sub stepsTo {
    my ( $replacements, $molecule ) = @_;

    my @elems = sort { length $b <=> length $a } keys %$replacements;

    my $steps = 0;

    while( $molecule ne "e" ) {
        foreach my $elem ( @elems ) {
            if ( $molecule =~ m/$elem/ ) {
                $molecule =~ s/$elem/$replacements->{$elem}/;
                last;
            }
        }
        $steps++;
    }

    return $steps;
}

1;
