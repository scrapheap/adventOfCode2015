package Day_10;

use Modern::Perl;

use Exporter qw( import );

our @EXPORT_OK = qw( lookAndSay );


sub lookAndSay {
    my ( $string ) = @_;

    my @groups = $string =~ m/(0+|1+|2+|3+|4+|5+|6+|7+|8+|9+)/g;

    return join( '', map {  ( length $_ ) . substr( $_, 0, 1 )  } @groups );

};

1;
