package Day_06;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( region parseLine );

sub region {
    my ( $minX, $minY, $maxX, $maxY ) = @_;

    my @coords;

    foreach my $y ( $minY..$maxY ) {
        foreach my $x ( $minX..$maxX ) {
            push @coords, [ $x, $y ];
        }
    }

    return @coords;
}

sub parseLine {
    my ($line) = @_;

    my ($op, $fromX, $fromY, $toX, $toY) = $line =~ m/(turn on|toggle|turn off)\s+(\d+),(\d+)\s+through\s+(\d+),(\d+)/;

    return $op
        ? {
            op => $op,
            from => [ $fromX, $fromY ],
            to => [ $toX, $toY ],
        }
        : ();
}

1;
