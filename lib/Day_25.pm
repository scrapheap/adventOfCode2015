package Day_25;

use Modern::Perl;

use Exporter qw( import );

our @EXPORT_OK = qw(
    nextCode
    codeAt
);

sub nextCode {
    my ( $seed ) = @_;

    return ( $seed * 252533 ) % 33554393;
}

sub codeAt {
    my ( $row, $col ) = @_;

    my $distance = 0;
    for( my $i = 1; $i <= $col; $i++) {
        $distance += $i;
    }
    
    my $inc = $col;
    for( my $i = 2; $i <= $row; $i++) {
        $distance += $inc;
        $inc++;
    }

    my $seed = 20151125;
    while ( $distance > 1 ) {
        $seed = nextCode( $seed );
        $distance--;
    }

    return $seed;
}
1;
