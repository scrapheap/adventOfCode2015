package Day_03;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( deliverPresents splitRoute );

sub deliverPresents {
    my @directions = split //, shift;
    my $grid = shift // {0 => { 0 => 1 } };

    my %move = (
        '>' => sub { ( $_[0] + 1, $_[1]     ) },
        '<' => sub { ( $_[0] - 1, $_[1]     ) },
        '^' => sub { ( $_[0],     $_[1] + 1 ) },
        'v' => sub { ( $_[0],     $_[1] - 1 ) },
    );

    my ( $x, $y ) = ( 0, 0 );

    foreach my $direction (@directions) {
        ( $x, $y ) = $move{ $direction }->( $x, $y );
        $grid->{$x}->{$y}++;
    }

    return $grid;
}

sub splitRoute {
    my @directions = split //, shift;

    my @routes = ( [], [] );
    my $i = 0;

    while ( @directions ) {
        push @{ $routes[ $i % 2 ] }, shift @directions;
        $i++;
    }


    return map { join("", @{ $_ } ) } @routes;
}
