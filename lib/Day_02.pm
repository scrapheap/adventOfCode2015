package Day_02;

use Modern::Perl;
use Exporter qw( import );

use List::Util qw( min sum );

our @EXPORT_OK = qw( paperRequirement ribbonRequirement );

sub paperRequirement {
    my ($length, $width, $height) = split /x/i, shift;

    my @sides = (
        $length * $width,
        $width * $height,
        $height * $length
    );
    
    return sum( @sides, @sides, min( @sides ) );
}

sub ribbonRequirement {
    my ($length, $width, $height) = split /x/i, shift;

    my @sides = (
        ( $length + $width + $length + $width ),
        ( $length + $height + $length + $height ),
        ( $width + $height + $width + $height ),
    );

    my $bowLength = $length * $width * $height;

    return min( @sides ) + $bowLength;
}
