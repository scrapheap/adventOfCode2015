package Day_13;

use Modern::Perl;

use Exporter qw( import );

our @EXPORT_OK = qw(
    optimalHappiness
);

use List::Permutor;
use List::Util qw( max );

sub optimalHappiness {
    my @guestInfo = @_;

    my $guests = {};

    foreach my $line ( @guestInfo ) {
        my ( $guest, $effect, $value, $target )
            = $line =~ m/(\w+) would (\w+) (\d+) happiness units by sitting next to (\w+)\./;

        $guests->{ $guest }->{ $target } =  $effect eq "lose"  ?  -$value  :  $value;
    }

    my $permutations = List::Permutor->new( keys %$guests );

    my @happinessLevels;

    while ( my @permutation = $permutations->next ) {
        push @happinessLevels, happiness( $guests, @permutation );
    }

    return max( @happinessLevels );
}

sub happiness {
    my ( $guests, @permutation ) = @_;

    my $total = 0;

    my $prev = shift @permutation;
    push @permutation, $prev;

    while ( my $next = shift @permutation ) {
        $total += $guests->{$prev}->{$next};
        $total += $guests->{$next}->{$prev};

        $prev = $next;
    }

    return $total;
}

1;
