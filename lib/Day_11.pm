package Day_11;

use Modern::Perl;

use Exporter qw( import );

our @EXPORT_OK = qw(
    validCharacters  containsStraight  containsAtLeastTwoDoubles  nextValidPassword
    jumpToEndOfInvalidCharacterRange
);


sub validCharacters {
    my ( $string ) = @_;

    return $string !~ m/[ilo]/;
}


sub containsStraight {
    my ( $string ) = @_;

    my @chars = map { ord( $_ ) } split //, $string;

    my $prev = shift @chars;
    my $run = 0;


    while( my $next = shift @chars ) {
        if ( $next - $prev == 1 ) {
            $run++;
            if ( $run >= 2 ) {
                return 1;
            }
        } else {
            $run = 0;
        }
        $prev = $next;
    }
    
    return 0;
}


sub containsAtLeastTwoDoubles {
    my ( $string ) = @_;
    
    return $string =~m/(.)\1.*(.)\2/;
}


sub jumpToEndOfInvalidCharacterRange {
    my ( $string ) = @_;

    my ( $start, $end ) = $string =~ m/^(.*?[ilo])(.*)$/;

    return 
        $start
            ? $start . ( 'z' x length $end )
            : $string;
}


sub nextValidPassword {
    my ( $password ) = @_;

    do {
        $password++;
        if ( ! validCharacters( $password ) ) {
            $password = jumpToEndOfInvalidCharacterRange( $password );
        }
    } until (
        validCharacters( $password )
        && containsStraight( $password )
        && containsAtLeastTwoDoubles( $password )
    );

    return $password;
}

1;
