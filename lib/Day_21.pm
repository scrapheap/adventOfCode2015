package Day_21;

use Modern::Perl;

use Exporter qw( import );

our @EXPORT_OK = qw(
    fight  equipmentGenerator

    BOSS_WINS  PLAYER_WINS
);

use List::Util qw( max );

use constant BOSS_WINS   => 0;
use constant PLAYER_WINS => 1;

sub fight {
    my ( $player, $boss ) = @_;

    while ( 1 ) {
        $boss->{hp} -= max( 1, $player->{damage} - $boss->{armour} );
        if( $boss->{hp} < 1 ) {
            return PLAYER_WINS;
        }

        $player->{hp} -= max( 1, $boss->{damage} - $player->{armour} );
        if( $player->{hp} < 1 ) {
            return BOSS_WINS;
        }
    }
}


sub equipmentGenerator {
    my %options = @_;

    my @weapons = @{ $options{weapons} };
    my @armour  = @{ $options{armour} };
    my @rings   = @{ $options{rings} };

    my $weapon = 0;
    my $armour = -1;
    my $ring1  = -1;
    my $ring2  = -1;

    return sub {
        my $player = {
            equipment => [],
            cost      => 0,
            damage    => 0,
            armour    => 0,
        };

        if ( $weapon > $#weapons ) {
            return undef;
        }

        $player = addItem( $player, $weapons[ $weapon ] );

        if ( $armour >= 0 ) {
            $player = addItem( $player, $armour[ $armour ] );
        }

        if ( $ring1 >= 0 ) {
            $player = addItem( $player, $rings[ $ring1 ] );
        }

        if ( $ring2 >= 0 ) {
            $player = addItem( $player, $rings[ $ring2 ] );
        }

        $ring2++;
        if ( $ring2 == $ring1 ) {
            $ring2++;
        }

        if ( $ring2 > $#rings ) {
            $ring1++;
            $ring2 = $ring1 + 1;

            if ( $ring1 > ( $#rings - 1 ) ) {
                $ring1 = -1;
                $ring2 = -1;
                $armour++;

                if ( $armour > $#armour ) {
                    $armour = -1;
                    $weapon++;
                }
            }
        }
        
        return $player;
    };
}

sub addItem {
    my ( $player, $item ) = @_;

    push @{ $player->{equipment} }, $item->{name};
    $player->{cost}   += $item->{cost};
    $player->{damage} += $item->{damage};
    $player->{armour} += $item->{armour};

    return $player;
}
1;
