package Day_23;

use Modern::Perl;

use Exporter qw( import );

our @EXPORT_OK = qw(
    runCode
);

our %baseRegisters = ( a => 0, b => 0, pc => 0 );

my %ops = (
    hlf => sub {
        my ( $register, $reg ) = @_;
        $register->{ $reg } = int( $register->{ $reg } / 2 );
        return $register;
    },
    tpl => sub {
        my ( $register, $reg ) = @_;
        $register->{ $reg } *= 3;
        return $register;
    },
    inc => sub {
        my ( $register, $reg ) = @_;
        $register->{ $reg }++;
        return $register;
    },
    jmp => sub {
        my ( $register, $offset ) = @_;
        $register->{pc} += ( $offset - 1 );
        return $register;
    },
    jie => sub {
        my ( $register, $reg, $offset ) = @_;
        if ( $register->{ $reg } % 2 == 0 ) {
            $register->{pc} += ( $offset - 1 );
        }
        return $register;
    },
    jio => sub {
        my ( $register, $reg, $offset ) = @_;
        if ( $register->{ $reg } == 1 ) {
            $register->{pc} += ( $offset - 1 );
        }
        return $register;
    },
);


sub runCode {
    my @code = @_;

    my $register = { %baseRegisters };

    while ( my $instruction = $code[ $register->{pc} ] ) {
        my ( $op, @operands ) = grep { $_ } split /[ ,]/, $instruction =~ s/[\r\n]+//r;
        $register = $ops{ $op }->( $register, @operands );
        $register->{pc}++;
    }

    return ( $register->{a}, $register->{b} );
};

1;
