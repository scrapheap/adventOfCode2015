package Day_18;

use Modern::Perl;

use Exporter qw( import );

our @EXPORT_OK = qw(
    parseGrid
    nextGrid
    turnOnCorners
);


use List::Util qw( min max sum );

sub parseGrid {
    my @schema = @_;

    my @grid = map { gridLine( $_ ) } @schema;

    return @grid;
}


sub gridLine {
    my ( $line ) = @_;

    my %val = ( '.' => 0, '#' => 1 );
    return [ map { $val{ $_ } // () } split //, $line ];
}


sub nextGrid {
    my @grid = @_;

    my $rows = scalar @grid;
    my $cols = scalar @{$grid[0]};

    my $neighboursOf = neighbourFinder( $rows, $cols );
    my @nextGrid;

    for( my $row = 0; $row < $rows; $row++ ) {
        for( my $col = 0; $col < $cols; $col++ ) {
            my $neighboursOn = sum( map { $grid[ $_->[0] ][ $_->[1] ] } $neighboursOf->( $row, $col ) );

            $nextGrid[ $row ][ $col ] = ($neighboursOn == 3 || ( $grid[$row][$col] && $neighboursOn == 2 ) ) ? 1 : 0;
        }
    }

    return @nextGrid;
}


sub neighbourFinder {
    my ( $rows, $cols ) = @_;

    return sub {
        my ( $row, $col ) = @_;

        my @neighbours;

        for( my $nRow = max( $row - 1, 0 ); $nRow < min( $rows, $row + 2 ); $nRow++ ) {
            for( my $nCol = max( $col - 1, 0 ); $nCol < min( $cols, $col + 2 ); $nCol++ ) {
                push @neighbours, [ $nRow, $nCol ]  unless  $nRow == $row && $nCol == $col;
            }
        }

        return @neighbours;
    };
}

sub turnOnCorners {
    my @grid = @_;

    my $rows = scalar @grid;
    my $cols = scalar @{$grid[0]};

    my @nextGrid = map { [ @$_ ] } @grid;

    $nextGrid[0][0]                     = 1;
    $nextGrid[0][ $cols - 1 ]           = 1;
    $nextGrid[ $rows - 1 ][0]           = 1;
    $nextGrid[ $rows - 1 ][ $cols - 1 ] = 1;

    return @nextGrid;
}
1;
