package Day_07;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( runCircuit );

my %ops = (
    AND    => sub { $_[0] & $_[1] },
    OR     => sub { $_[0] | $_[1] },
    NOT    => sub { ( ~$_[0] ) & 0xFFFF },
    LSHIFT => sub { ( $_[0] << $_[1] ) & 0xFFFF },
    RSHIFT => sub { $_[0] >> $_[1] },
);

sub runCircuit {
    my @schema = @_;

    my %wire;

    while ( my $line = shift @schema ) {
        my ( $input, $output ) = $line =~ m/^\s*(.*?)\s*->\s*(.*?)\s*$/;

        if ( $input =~ m/^\w+$/ ) {
            my $value = ( $input =~ m/^\d+$/  ?  int $input  :  $wire{ $input } );
            if( defined $value ) {
                $wire{ $output } = $value;
            } else {
                push @schema, $line;
                next;
            }
        } else {
            my ( $inWire1, $op, $inWire2 ) = $input =~ m/(?:(\w+)\s+)?(\w+)\s+(\w+)/;

            next unless $op;

            my @operands;
            if( defined $inWire1 ) {
                my $value = ( $inWire1 =~ m/^\d+$/  ?  int $inWire1  :  $wire{ $inWire1 } );
                if ( defined $value ) {
                    push @operands, $value;
                } else {
                    push @schema, $line;
                    next;
                }
            }
            if( defined $inWire2 ) {
                my $value = ( $inWire2 =~ m/^\d+$/  ?  int $inWire2  :  $wire{ $inWire2 } );
                if ( defined $value ) {
                    push @operands, $value;
                } else {
                    push @schema, $line;
                    next;
                }
            }

            $wire{$output} = $ops{ $op }->( @operands );
        }
    }

    return %wire;
};

1;
