package Day_24;

use Modern::Perl;

use Exporter qw( import );

our @EXPORT_OK = qw(
    quantumEntanglement
    optimalSplit
    optimalSplitWithTrunk
);

use List::Util qw( reduce sum );

sub quantumEntanglement {
    return reduce { $a * $b } 1, @_;
}

sub optimalSplit {
    my @weights = sort { $b <=> $a } @_;

    my $target = sum( @weights ) / 3;

    my $group1 = find( $target, [], @weights );

    @weights = drop( $group1, @weights );

    my $group2 = find( $target, [], @weights );

    my $group3 = [ drop( $group2, @weights ) ];


    return sort { cmpGroup( $a, $b ) } ( $group1, $group2, $group3 );
}

sub optimalSplitWithTrunk {
    my @weights = sort { $b <=> $a } @_;

    my $target = sum( @weights ) / 4;

    my @attempts = find( $target, [], @weights );

    my @previous;
    my @onhold;

    for( 1..5 ) {
        push @attempts, find( $target, [], @previous, @weights );
        if ( @onhold ) {
            push @previous, shift @onhold;
        }
        push @onhold, shift @weights;
    }

    @weights = ( @previous, @onhold, @weights );

    @previous = ();
    @onhold = ( shift @weights, shift @weights );

    for( 1..5 ) {
        push @attempts, find( $target, [], @previous, @weights );
        if ( @onhold ) {
            push @previous, shift @onhold;
        }
        push @onhold, shift @weights;
    }

    @attempts = sort { cmpGroup( $a, $b ) } @attempts;

    return $attempts[0];
}


sub find {
    my ( $target, $group, @weights ) = @_;

    if ( $target == 0 ) {
        return $group
    }

    while( my $weight = shift @weights ) {    
        next if $weight > $target;
        my $found = find( $target - $weight, [ @$group, $weight ], @weights );
        if ( $found ) {
            return $found;
        }
    }

    return undef;
}

sub drop {
    my ( $exclusions, @values ) = @_;

    my %exclude = map { $_ => 1 } @$exclusions;

    return grep { ! $exclude{ $_ } } @values;

}

sub cmpGroup {
    my ( $a, $b ) = @_;

    if ( scalar @$a < scalar @$b ) {
        return -1;
    }
    if ( scalar @$a > scalar @$b ) {
        return 1;
    }
    return quantumEntanglement( @$a ) <=> quantumEntanglement( @$b );
}

1;
