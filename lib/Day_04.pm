package Day_04;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( findValidMD5 );

use Digest::MD5 qw( md5_hex );

sub findValidMD5 {
    my ( $input, $pattern ) = @_;

    $pattern //= qr/^00000/;

    my $i = 1;

    while ( md5_hex("$input$i") !~ m/$pattern/ ) {
        $i++;
    };

    return $i;
}

1;
