package Day_08;

use Modern::Perl;
use Exporter qw( import );

use List::Util qw( sum0 );

our @EXPORT_OK = qw( escapeString  unescapeString  totalStringsSize  totalMemoryUsed );


sub escapeString {
    my ( $string ) = @_;

    return '' unless $string;

    return '"' . ( $string =~ s/([\\"])/\\$1/gr ) . '"';
}

sub unescapeString {
    my ( $string ) = @_;
    return eval $string;
}

sub totalStringsSize {
    return sum0( map { length } @_ );
}

sub totalMemoryUsed {
    return sum0( map { length unescapeString( $_ ) } @_ );
}

1;
