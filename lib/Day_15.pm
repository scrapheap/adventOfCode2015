package Day_15;

use Modern::Perl;

use Exporter qw( import );

our @EXPORT_OK = qw(
    calculateMaxTastiness
    calculateMaxTastinessForCalories
);

use List::Util qw( max );


sub calculateMaxTastiness {
    my ( @ingredientInfo ) = @_;

    my @ingredients;

    foreach my $line ( @ingredientInfo ) {
        my ( $name, $rest ) = $line =~ m/(\w+): (.*?)[\r\n]*$/;

        push @ingredients, { map { split /\s+/ } split /,\s*/, $rest };
    }

    my $maxTastiness = 0;

    loop( 100, scalar @ingredients, sub {
        my @teaspoons = @_;

        my $tastiness = 1;
        foreach my $property ( qw( capacity durability flavor texture ) ) {
            my $prop = 0;

            for my $i ( 0..$#ingredients ) {
                next unless $teaspoons[ $i ];

                $prop += $teaspoons[ $i ] * $ingredients[ $i ]->{ $property };
            }

            $tastiness *= max( $prop, 0 );
        }

        $maxTastiness = max( $tastiness, $maxTastiness );
    } );

    return $maxTastiness;
}


sub calculateMaxTastinessForCalories {
    my ( $calories, @ingredientInfo ) = @_;

    my @ingredients;

    foreach my $line ( @ingredientInfo ) {
        my ( $name, $rest ) = $line =~ m/(\w+): (.*?)[\r\n]*$/;

        push @ingredients, { map { split /\s+/ } split /,\s*/, $rest };
    }

    my $maxTastiness = 0;

    loop( 100, scalar @ingredients, sub {
        my @teaspoons = @_;

        my $tastiness = 1;
        foreach my $property ( qw( capacity durability flavor texture ) ) {
            my $prop = 0;

            for my $i ( 0..$#ingredients ) {
                next unless $teaspoons[ $i ];

                $prop += $teaspoons[ $i ] * $ingredients[ $i ]->{ $property };
            }

            $tastiness *= max( $prop, 0 );
        }

        my $cals = 0;
        for my $i ( 0..$#ingredients ) {
            next unless $teaspoons[ $i ];

            $cals += $teaspoons[ $i ] * $ingredients[ $i ]->{calories};
        }

        if( $cals == $calories ) {
            $maxTastiness = max( $tastiness, $maxTastiness );
        };
    } );

    return $maxTastiness;
}


sub loop {
    my ( $left, $count, $sub, @tries ) = @_;

    if ( $count ) {
        foreach my $try ( 0..$left ) {
            loop( $left - $try, $count - 1, $sub, @tries, $try );
        }
    } else {
        $sub->( @tries );
    }
}

1;
