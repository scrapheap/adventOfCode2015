package Day_17;

use Modern::Perl;

use Exporter qw( import );

our @EXPORT_OK = qw(
    findAllPermutations
    findSmallestSets
);

use List::Util qw( sum min );

sub findAllPermutations {
    my ( $quantity, @containers ) = @_;

    my $containerCount = scalar @containers;
    my $max = 2**$containerCount - 1;

    my @permutations;

    foreach my $permutation ( 1..$max ) {
        my @set;

        foreach my $container ( 0..$containerCount ) {
            if ( $permutation & ( 1 << $container ) ) {
                push @set, $containers[ $container ];
            }
        }

        push @permutations, \@set if sum( @set ) == $quantity;
    }

    return @permutations;
}


sub findSmallestSets {
    my @sets = @_;

    my $min = min( map { scalar @$_ } @sets );

    my @minSets = grep { $min == scalar @$_ } @sets;

    return @minSets;
}

1;
