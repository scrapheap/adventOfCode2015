package Day_16;

use Modern::Perl;

use Exporter qw( import );

our @EXPORT_OK = qw(
    createFilter
    createFilter2
);

use List::Util qw( any );

sub createFilter {
    my %criteria = map { m/(\w+)\s*:\s*(\d+)/ } @_;

    return sub {
        my ( $line ) = @_;

        my ( $details ) = $line =~ m/sue\s+\d+\s*:\s*(.*)/i;
        my %details = map { m/(\w+)\s*:\s*(\d+)/ } split /\s*,\s*/, $details;

        foreach my $sample ( keys %criteria ) {
            if ( exists $details{ $sample }  &&  $criteria{ $sample } != $details{ $sample } ) {
                return 0;
            }
        }

        return 1;
    }
}

sub createFilter2 {
    my %criteria = map { m/(\w+)\s*:\s*(\d+)/ } @_;

    my %op = (
        default     => sub { $_[0] != $_[1] },
        cats        => sub { $_[0] <= $_[1] },
        trees       => sub { $_[0] <= $_[1] },
        pomeranians => sub { $_[0] >= $_[1] },
        goldfish    => sub { $_[0] >= $_[1] },
    );

    return sub {
        my ( $line ) = @_;

        my ( $details ) = $line =~ m/sue\s+\d+\s*:\s*(.*)/i;
        my %details = map { m/(\w+)\s*:\s*(\d+)/ } split /\s*,\s*/, $details;

        foreach my $sample ( keys %criteria ) {
            if ( exists $details{ $sample }  && ($op{ $sample } // $op{ default })->( $details{ $sample }, $criteria{ $sample } ) ) {
                return 0;
            }
        }

        return 1;
    }
}

1;
