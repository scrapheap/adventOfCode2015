package Day_22;

use Modern::Perl;

use Exporter qw( import );

our @EXPORT_OK = qw(
    findEfficientWinCost
);

use List::Util qw( min );

our $mode = "easy";

my %cost = (
    missile  => 53,
    drain    => 73,
    shield   => 113,
    poison   => 173,
    recharge => 229,
);

my %actions = (
    missile => sub {
        my ( $player, $boss, $timers, $manaSpent ) = map { clone( $_ ) } @_;

        $player->{mana} -= $cost{missile};
        $manaSpent += $cost{missile};
        $boss->{hp} -= 4;

        return ( $player, $boss, $timers, $manaSpent );
    },

    drain   => sub {
        my ( $player, $boss, $timers, $manaSpent ) = map { clone( $_ ) } @_;

        $player->{mana} -= $cost{drain};
        $manaSpent += $cost{drain};
        $boss->{hp} -= 2;
        $player->{hp} += 2;

        return ( $player, $boss, $timers, $manaSpent );
    },

    shield  => sub {
        my ( $player, $boss, $timers, $manaSpent ) = map { clone( $_ ) } @_;

        $player->{mana} -= $cost{shield};
        $manaSpent += $cost{shield};
        $timers->{shield} = 6;

        return ( $player, $boss, $timers, $manaSpent );
    },

    poison  => sub {
        my ( $player, $boss, $timers, $manaSpent ) = map { clone( $_ ) } @_;

        $player->{mana} -= $cost{poison};
        $manaSpent += $cost{poison};
        $timers->{poison} = 6;

        return ( $player, $boss, $timers, $manaSpent );
    },

    recharge => sub {
        my ( $player, $boss, $timers, $manaSpent ) = map { clone( $_ ) } @_;

        $player->{mana} -= $cost{recharge};
        $manaSpent += $cost{recharge};
        $timers->{recharge} = 5;

        return ( $player, $boss, $timers, $manaSpent );
    },
);

sub findEfficientWinCost {
    my ( $player, $boss ) = @_;

    my $timers = {
        missile  => 0,
        drain    => 0,
        shield   => 0,
        poison   => 0,
        recharge => 0,
    };

    return min( grep { defined } playerTurn( $player, $boss, $timers, 0, 11 ) );
}

sub clone {
    return ref $_[0] eq "HASH" ? { %{$_[0]} } : $_[0];
}

sub playerTurn {
    my ( $player, $boss, $timers, $manaSpent, $maxDepth ) = map { clone( $_ ) } @_;

    if ( $maxDepth < 1 ) {
        return undef;
    }

    if ( $mode eq "hard" ) {
        $player->{hp}--;
        if ( $player->{hp} < 1 ) {
            return undef;
        }
    }

    # Apply effects
    if ( $timers->{shield} ) {
        $player->{armour} = 7;
        $timers->{shield}--;
    } else {
        $player->{armour} = 0;
    }
    
    if ( $timers->{poison} ) {
        $boss->{hp} -= 3;
        $timers->{poison}--;
    }

    if ( $timers->{recharge} ) {
        $player->{mana} += 101;
        $timers->{recharge}--;
    }

    if ( $boss->{hp} < 1 ) {
        return $manaSpent;
    }

    my @attempts;

    foreach my $action ( availableActions( $player, $timers ) ) {
        push @attempts, bossTurn( $actions{ $action }->( $player, $boss, $timers, $manaSpent ), $maxDepth - 1 );
    }

    return @attempts;
}

sub bossTurn {
    my ( $player, $boss, $timers, $manaSpent, $maxDepth ) = map { clone( $_ ) } @_;

    # Apply effects
    if ( $timers->{shield} ) {
        $player->{armour} = 7;
        $timers->{shield}--;
    } else {
        $player->{armour} = 0;
    }
    
    if ( $timers->{poison} ) {
        $boss->{hp} -= 3;
        $timers->{poison}--;
    }

    if ( $timers->{recharge} ) {
        $player->{mana} += 101;
        $timers->{recharge}--;
    }

    if ( $boss->{hp} < 1 ) {
        return $manaSpent;
    }

    $player->{hp} -= $boss->{damage} - $player->{armour};

    if ( $player->{hp} < 1 ) {
        return undef;
    }

    playerTurn( $player, $boss, $timers, $manaSpent, $maxDepth );
};


sub availableActions {
    my ( $player, $timers ) = @_;

    my @actions = grep { $timers->{ $_ } == 0 } keys %$timers;

    return grep { $cost{ $_ } <= $player->{mana} } @actions;
}

1;
