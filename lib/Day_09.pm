package Day_09;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( shortestDistance  longestDistance );

use List::Permutor;
use List::Util qw( min max );
use Memoize;
memoize "permutationDistances";

sub shortestDistance {
    return min( permutationDistances( @_ ) );
}

sub longestDistance {
    return max( permutationDistances( @_ ) );
}

sub permutationDistances {
    my @distances = @_;

    my %map;

    foreach my $route ( @distances ) {
        my ( $from, $to, $distance ) = $route =~ m/(\w+)\s+to\s+(\w+)\s*=\s*(\d+)/;

        $map{$from}->{$to} = $distance;
        $map{$to}->{$from} = $distance;
    }

    my @locations = keys %map;

    my $permutations = List::Permutor->new( keys %map );

    my @routeLengths;

    while ( my @permutation = $permutations->next ) {
        my $total = 0;
        my $from = shift @permutation;

        while( my $to = shift @permutation ) {
            $total += $map{ $from }->{ $to };
            $from = $to;
        }

        push @routeLengths, $total;
    }

    return @routeLengths;
}

1;
