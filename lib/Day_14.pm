package Day_14;

use Modern::Perl;

use Exporter qw( import );

our @EXPORT_OK = qw(
    calculateMaxDistance
    calculatePoints
);

use List::Util qw( max );

sub calculateMaxDistance {
    my ( $seconds, @reindeerInfo ) = @_;

    my $reindeer = {};

    foreach my $line ( @reindeerInfo ) {
        my ( $deer, $distance, $flyFor, $restFor )
            = $line =~ m/(\w+) can fly (\d+) km\/s for (\d+) seconds, but then must rest for (\d+) seconds\./;

        $reindeer->{ $deer } = {
            distance => $distance,
            flyFor => $flyFor,
            restFor => $restFor,
            state => "flying",
            count => 0,
            total => 0,
        };
    }

    for ( 1..$seconds ) {
        foreach my $current ( keys %$reindeer ) {
            $reindeer->{ $current }->{count}++;
            if ( $reindeer->{ $current }->{state} eq "flying" ) {
                $reindeer->{ $current }->{total} += $reindeer->{ $current }->{distance};
                if ( $reindeer->{ $current }->{count} >= $reindeer->{ $current }->{flyFor} ) {
                    $reindeer->{ $current }->{count} = 0;
                    $reindeer->{ $current }->{state} = "resting";
                }

            } else {
                if ( $reindeer->{ $current }->{count} >= $reindeer->{ $current }->{restFor} ) {
                    $reindeer->{ $current }->{count} = 0;
                    $reindeer->{ $current }->{state} = "flying";
                }
            }
        }
    }

    return max( map { $_->{total} } values %$reindeer );
}

sub calculatePoints {
    my ( $seconds, @reindeerInfo ) = @_;

    my $reindeer = {};

    foreach my $line ( @reindeerInfo ) {
        my ( $deer, $distance, $flyFor, $restFor )
            = $line =~ m/(\w+) can fly (\d+) km\/s for (\d+) seconds, but then must rest for (\d+) seconds\./;

        $reindeer->{ $deer } = {
            distance => $distance,
            flyFor => $flyFor,
            restFor => $restFor,
            state => "flying",
            count => 0,
            points => 0,
            total => 0,
        };
    }

    for ( 1..$seconds ) {
        foreach my $current ( keys %$reindeer ) {
            $reindeer->{ $current }->{count}++;
            if ( $reindeer->{ $current }->{state} eq "flying" ) {
                $reindeer->{ $current }->{total} += $reindeer->{ $current }->{distance};
                if ( $reindeer->{ $current }->{count} >= $reindeer->{ $current }->{flyFor} ) {
                    $reindeer->{ $current }->{count} = 0;
                    $reindeer->{ $current }->{state} = "resting";
                }

            } else {
                if ( $reindeer->{ $current }->{count} >= $reindeer->{ $current }->{restFor} ) {
                    $reindeer->{ $current }->{count} = 0;
                    $reindeer->{ $current }->{state} = "flying";
                }
            }
        }

        my $currentWinningDistance = max( map { $_->{total} } values %$reindeer );

        foreach my $current ( keys %$reindeer ) {
            if( $reindeer->{ $current }->{total} == $currentWinningDistance ) {
                $reindeer->{ $current }->{points}++;
            }
        }
    }

    return max( map { $_->{points} } values %$reindeer );
}

1;
